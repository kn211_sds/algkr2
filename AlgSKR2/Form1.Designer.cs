﻿
namespace AlgSKR2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListBox = new System.Windows.Forms.ListBox();
            this.dexroz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListBox
            // 
            this.ListBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ListBox.FormattingEnabled = true;
            this.ListBox.ItemHeight = 18;
            this.ListBox.Location = new System.Drawing.Point(351, 27);
            this.ListBox.Name = "ListBox";
            this.ListBox.Size = new System.Drawing.Size(322, 328);
            this.ListBox.TabIndex = 0;
            // 
            // dexroz
            // 
            this.dexroz.BackColor = System.Drawing.Color.LightSalmon;
            this.dexroz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dexroz.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dexroz.Location = new System.Drawing.Point(40, 27);
            this.dexroz.Name = "dexroz";
            this.dexroz.Size = new System.Drawing.Size(272, 51);
            this.dexroz.TabIndex = 1;
            this.dexroz.Text = "Знайти";
            this.dexroz.UseVisualStyleBackColor = false;
            this.dexroz.Click += new System.EventHandler(this.dexroz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.IndianRed;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dexroz);
            this.Controls.Add(this.ListBox);
            this.Name = "Form1";
            this.Text = "Контрольна робота Алгоритми 2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox ListBox;
        private System.Windows.Forms.Button dexroz;
    }
}

